﻿using System;
using Newtonsoft.Json;

namespace SensorGateway.Data
{
    public static class Consts
    {
        public const int TEMPERATURE_MIN = -50;
        public const int TEMPERATURE_MAX = 50;
        public const double TEMPERATURE_VAR_RATIO = 1.0 / 3.0;

        public const int WINDSPD_MIN = 0;
        public const int WINDSPD_MAX = 408;
        public const int WINDSPD_VAR_RATIO = 10;

        public const int WINDGUST_VAR_MIN = 0;
        public const int WINDGUST_VAR_MAX = 30;

        public const double WINDDIR_MIN = 0;
        public const double WINDDIR_MAX = 360;
        public const double WINDDIR_VAR_RATIO = 1;

        public const double PRESS_MIN = 980;
        public const double PRESS_MAX = 1080;
        public const double PRESS_VAR_RATIO = 10;

        public const double HUM_MIN = 0;
        public const double HUM_MAX = 70;
        public const double HUM_VAR_RATIO = 0.8;
    }

    public class WeatherData
    {
        [JsonProperty("position")]
        public Position Position { get; set; }

        [JsonProperty("weather_info")]
        public WeatherInfo WeatherInfo { get; set; }
    }

    public class Position
    {
        [JsonProperty("latitude")]
        public double Latitude { get; set; }

        [JsonProperty("longitude")]
        public double Longitude { get; set; }

        [JsonProperty("elevation")]
        public double Elevation { get; set; }
    }

    public class WeatherInfo
    {
        [JsonProperty("datetime")]
        public DateTimeOffset DateTime { get; set; }

        [JsonProperty("temperature")]
        public double Temperature { get; set; }

        [JsonProperty("wind_speed")]
        public double WindSpeed { get; set; }

        [JsonProperty("wind_gust")]
        public double WindGust { get; set; }

        [JsonProperty("wind_dir")]
        public double WindDir { get; set; }

        [JsonProperty("pressure")]
        public double Pressure { get; set; }

        [JsonProperty("humidity")]
        public double Humidity { get; set; }
    }
}