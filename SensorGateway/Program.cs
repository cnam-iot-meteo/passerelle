﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using SensorGateway.Data;
using static SensorGateway.Data.Consts;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Linq;
using System.Text;
using System.Threading;

namespace SensorGateway
{
    internal static class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length < 3)
            {
                Console.Error.WriteLine("ARGS: host port conf1.json [conf2.json ...]");
                Environment.Exit(1);
            }
            var rand = new Random();

            var host = args[0];
            var port = args[1];

            // Load fake gateway conf
            List<WeatherData> gateways = new List<WeatherData>();
            foreach (var gtw in args.Skip(2))
            {
                try
                {
                    var position = JsonConvert.DeserializeObject<Position>(File.ReadAllText(gtw));
                    var data = new WeatherData();
                    data.Position = position;
                    data.WeatherInfo = new WeatherInfo();
                    gateways.Add(data);
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine($"Could not parse {gtw}: {e.Message}");
                }
            }

            if (!gateways.Any())
            {
                Console.Error.WriteLine("No configuration loaded.");
                Environment.Exit(1);
            }

            // Generate fake data
            foreach (var data in gateways)
            {
                data.WeatherInfo.Temperature = rand.NextDouble() * (TEMPERATURE_MAX - TEMPERATURE_MIN) + TEMPERATURE_MIN;
                data.WeatherInfo.Humidity = (int)Math.Round(rand.NextDouble() * HUM_MAX);
                data.WeatherInfo.Pressure = rand.NextDouble() * (PRESS_MAX - PRESS_MIN) + PRESS_MIN;
                data.WeatherInfo.WindSpeed = (int)Math.Round(rand.NextDouble() * (WINDSPD_MAX - WINDSPD_MIN) + WINDSPD_MIN);
                data.WeatherInfo.WindDir = Math.Round(rand.NextDouble() * WINDDIR_MAX, 2);
                data.WeatherInfo.WindGust = data.WeatherInfo.WindSpeed + rand.Next(WINDGUST_VAR_MIN, WINDGUST_VAR_MAX);
            }

            // Apply variation and push fake data at time interval
            while (true)
            {
                foreach (var data in gateways)
                {
                    data.WeatherInfo.DateTime = DateTime.UtcNow;
                    data.WeatherInfo.Temperature = Math.Round(data.WeatherInfo.Temperature + ((rand.NextDouble() * 2.0 - 1.0) / TEMPERATURE_VAR_RATIO), 1);
                    data.WeatherInfo.Humidity += (int)Math.Round((rand.NextDouble() * 2.0 - 1.0) / HUM_VAR_RATIO);
                    data.WeatherInfo.Pressure = Math.Round(data.WeatherInfo.Pressure + ((rand.NextDouble() * 2 - 1) / PRESS_VAR_RATIO), 2);
                    data.WeatherInfo.WindSpeed = Math.Round(data.WeatherInfo.WindSpeed + ((rand.NextDouble() * 2.0 - 1.0) / WINDSPD_VAR_RATIO), 1);
                    data.WeatherInfo.WindDir = Math.Round(data.WeatherInfo.WindDir + (rand.NextDouble() * 2.0 - 1.0), 2);
                    data.WeatherInfo.WindGust = data.WeatherInfo.WindSpeed + rand.Next(WINDGUST_VAR_MIN, WINDGUST_VAR_MAX);

                    try
                    {
                        using (var client = new HttpClient())
                        {
                            // Prepare URL
                            var uri = $"https://{host}:{port}/api/measures";

                            var json = JsonConvert.SerializeObject(data, Formatting.Indented);
                            Console.WriteLine(json);
                            Console.WriteLine();
                            Console.WriteLine();
                            var content = new StringContent(json, Encoding.UTF8, "application/json");

                            // Send POST to server
                            using (var response = client.PostAsync(uri, content).Result)
                            {
                                if (!response.IsSuccessStatusCode)
                                {
                                    Console.Error.WriteLine($"Server responded {response.StatusCode} : {response.ReasonPhrase}");
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.Error.WriteLine($"{e.Message}");
                    }
                }
                Thread.Sleep(1000);
            }
        }
    }
}